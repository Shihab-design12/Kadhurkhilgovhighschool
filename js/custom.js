/************************sticky animated navbar****************************/
$(document).ready(function(){
    $(window).on('scroll',function(){
        if($(window).scrollTop()>200){
            $('nav').addClass('black');
        } else{
            $('nav').removeClass('black');
        }
    })
})
/****************************Scroll-up Button***********************************/
mybutton=document.querySelector("#topBtn");
window.onscroll=function(){scrollFunction()};
function scrollFunction(){
    if(document.body.scrollTop>200||document.documentElement.scrollTop>200){
        mybutton.style.display="block";
    }else{mybutton.style.display="none";}
}
function topFunction(){
    document.body.scrollTop=0;//For Safari
    document.documentElement.scrollTop=0;//For Chrome,Internet explorer & opera
}
/******************************Wow animation Plugin*********************************/
wow = new WOW(
    {
      animateClass: 'animated',
      offset:       100,
      callback:     function(box) {
        console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
      }
    }
  );
  wow.init();
  document.getElementById('moar').onclick = function() {
    var section = document.createElement('section');
    section.className = 'section--purple wow fadeInDown';
    this.parentNode.insertBefore(section, this);
  };

